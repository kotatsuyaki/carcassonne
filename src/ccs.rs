use crate::io::AsImg;
use array2d::Array2D;
use arrayvec::ArrayVec;
use enum_iterator::IntoEnumIterator;
use itertools::iproduct;
use lazy_static::lazy_static;
use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::{FromPrimitive, ToPrimitive};
use rand::prelude::*;
use rayon::prelude::*;
use std::collections::{HashMap, HashSet};
use std::num::NonZeroU8;

use tensorflow as tf;

lazy_static! {
    pub static ref GET_ACTION_TIMES: std::sync::Mutex<Vec<std::time::Duration>> =
        std::sync::Mutex::new(vec![]);
    pub static ref DER_ACTION_TIMES: std::sync::Mutex<Vec<std::time::Duration>> =
        std::sync::Mutex::new(vec![]);
}

const BOARD_SIZE: usize = 16;

#[derive(Debug, Clone)]
pub struct State {
    /* Entity status members */
    board: Board,
    deck: Deck,
    remaining_meeples: [u8; 2],

    /* City-related members */
    /// Mapping from (Loc, Side) pair to the Id of the city
    city_ids: Array2D<[Option<NonZeroU8>; 4]>,
    /// Mapping from the Id's of the cities to their number of open ends
    city_open_sides: HashMap<NonZeroU8, u8>,
    next_city_id: NonZeroU8,
    exist_cities: HashSet<NonZeroU8>,
    /// Set of complete (closed) cities. This is used for the final field scoring.
    complete_cities: HashSet<NonZeroU8>,

    /* Road-related members */
    /// Mapping from (Loc, Side) pair to the Id of the road
    road_ids: Array2D<[Option<NonZeroU8>; 4]>,
    next_road_id: NonZeroU8,
    exist_roads: HashSet<NonZeroU8>,

    /* Field-related members */
    field_ids: Array2D<[Option<NonZeroU8>; 12]>,
    next_field_id: NonZeroU8,

    /* Misc members */
    next_player: Player,
    scores: [u16; 2],
    age: u8,
    last_action: Option<Action>,
    drawn_tile: Option<RawTile>,
}

impl State {
    pub fn new() -> Self {
        Self {
            board: Board::new(),
            deck: Deck::new(),
            remaining_meeples: [7, 7],
            /* city */
            city_ids: Array2D::filled_with([None; 4], 16, 16),
            city_open_sides: HashMap::new(),
            next_city_id: NonZeroU8::new(1).unwrap(),
            exist_cities: HashSet::new(),
            complete_cities: HashSet::new(),
            /* road */
            road_ids: Array2D::filled_with([None; 4], 16, 16),
            next_road_id: NonZeroU8::new(1).unwrap(),
            exist_roads: HashSet::new(),
            /* field */
            field_ids: Array2D::filled_with([None; 12], 16, 16),
            next_field_id: NonZeroU8::new(1).unwrap(),
            /* misc */
            next_player: Player::Red,
            scores: [0, 0],
            age: 0,
            last_action: None,
            drawn_tile: None,
        }
    }

    /// Convert the current state as a tensor. The viewpoint is from the perspective of
    /// `self.next_player`. `self.drawn_tile` must be present.
    ///
    /// Output tensor shapes:
    /// - `main_tensor`: `(16, 16, 110)`
    /// - `drawn_tensor`: `(24)`
    /// - `scores_tensor`: `(2)`
    pub fn as_tensor(&self) -> (tf::Tensor<f32>, tf::Tensor<f32>, tf::Tensor<f32>) {
        const OCCUPY_SHIFT: u64 = 0;
        const ONEHOT_SHIFT: u64 = 1;
        const CITY_SHIFT: u64 = 97;
        const ROAD_SHIFT: u64 = 101;
        const FIELD_SHIFT: u64 = 105;
        const CLOISTER_SHIFT: u64 = 109;

        let me = self.next_player;

        let mut main_tensor = tf::Tensor::new(&[16, 16, 110]);
        for (x, y) in iproduct!(0..16, 0..16) {
            let (x64, y64) = (x as u64, y as u64);

            if let Some(tile) = self.board[(x, y)] {
                // Occupation channel
                main_tensor.set(&[OCCUPY_SHIFT, x64, y64], 1.0);

                // Onehot channels
                let raw_tile_num: u8 = tile.raw_tile.to_u8().unwrap() - 1;
                let rotation_num = tile.rotation.to_u8().unwrap();
                let raw_tile_num = raw_tile_num as u64;
                let rotation_num = rotation_num as u64;
                main_tensor.set(
                    &[x64, y64, ONEHOT_SHIFT + raw_tile_num * 4 + rotation_num],
                    1.0,
                );

                // Meeple channels
                let player_to_num = |player| {
                    if player == me {
                        1.0
                    } else {
                        -1.0
                    }
                };
                match tile.meeple {
                    Some(Meeple {
                        kind: FeatureKind::City,
                        index,
                        player,
                    }) => main_tensor.set(
                        &[x64, y64, CITY_SHIFT + index as u64],
                        player_to_num(player),
                    ),
                    Some(Meeple {
                        kind: FeatureKind::Road,
                        index,
                        player,
                    }) => main_tensor.set(
                        &[x64, y64, ROAD_SHIFT + index as u64],
                        player_to_num(player),
                    ),
                    Some(Meeple {
                        kind: FeatureKind::Field,
                        index,
                        player,
                    }) => main_tensor.set(
                        &[x64, y64, FIELD_SHIFT + index as u64],
                        player_to_num(player),
                    ),
                    Some(Meeple {
                        kind: FeatureKind::Cloister,
                        index,
                        player,
                    }) => main_tensor.set(&[x64, y64, CLOISTER_SHIFT], player_to_num(player)),
                    _ => {}
                }
            }
        }

        let mut drawn_tensor = tf::Tensor::new(&[24]);
        if let Some(drawn_tile) = self.drawn_tile {
            let drawn_tile_num: usize = drawn_tile.to_u8().unwrap() as usize - 1;
            drawn_tensor[drawn_tile_num] = 1.0;
        }

        let scores_tensor = utils::scores_to_tensor(self.get_scores().clone(), me);

        (main_tensor, drawn_tensor, scores_tensor)
    }

    /// Draw a tile from `deck` and put to `drawn_tile`. Return `true` upon success.
    pub fn draw_tile(&mut self) -> bool {
        if let Some(raw_tile) = self.deck.draw() {
            self.drawn_tile = Some(raw_tile);
            true
        } else {
            false
        }
    }

    pub fn put_back_tile(&mut self, raw_tile: RawTile) {
        self.deck.put_back(raw_tile);
    }

    pub fn is_ended(&self) -> bool {
        self.deck.data.is_empty() && matches!(self.drawn_tile, None)
    }

    pub fn get_next_player(&self) -> Player {
        self.next_player
    }

    pub fn get_winner(&self) -> Option<Player> {
        match self.scores[Player::Red.idx()].cmp(&self.scores[Player::Blue.idx()]) {
            std::cmp::Ordering::Less => Some(Player::Blue),
            std::cmp::Ordering::Equal => None,
            std::cmp::Ordering::Greater => Some(Player::Red),
        }
    }

    pub fn get_scores(&self) -> &[u16; 2] {
        &self.scores
    }

    pub fn get_last_action(&self) -> Option<Action> {
        self.last_action
    }

    /// City floodfill algorithm. For all (loc, side) pairs connected to the city feature,
    ///
    /// - Update its corresponding `city_ids` entry as `new_city_id`
    /// - Add number of open sides encountered to the corresponding `city_open_sides` entry
    fn floodfill_city(
        city_ids: &mut Array2D<[Option<NonZeroU8>; 4]>,
        city_open_sides: &mut HashMap<NonZeroU8, u8>,
        board: &Board,
        exist_cities: &mut HashSet<NonZeroU8>,
        from_tile: Tile,
        from_loc: Loc,
        from_feat_idx: usize,
        new_city_id: NonZeroU8,
    ) {
        let mut stk = vec![];
        let ref resident_feat = from_tile.city_features()[from_feat_idx];
        for &side in resident_feat.iter() {
            stk.push((from_loc, side));
        }

        // Ensure that city_open_sides starts with 0 for this city Id
        city_open_sides.entry(new_city_id).or_insert(0);

        // DFS
        // `loc` and `side` are the "centeral" viewpoint of this iteration.
        // Attempt to floodfill all the adjacent features.
        while let Some((loc, side)) = stk.pop() {
            // Get mutable reference to the city Id entry for this (loc, side) pair.
            // Terminating condition: if the side is the same as `new_city_id`
            let city_id = match &mut city_ids[loc.into()][side.idx()] {
                // Same city case (terminate)
                Some(city_id) if *city_id == new_city_id => {
                    continue;
                }
                // Different city / no city case
                other_city_id_or_none => other_city_id_or_none,
            };
            // Different city case
            if let Some(city_id) = city_id {
                exist_cities.remove(city_id);
            }
            // Update its entry value to the new city Id
            *city_id = Some(new_city_id);

            // If it's not of edge, proceed. Otherwise, just increase open sides count
            if let Some(neighbor_loc) = loc.neighbor_on(side) {
                // If neighbor is not empty, proceed. Otherwise, increase open sides
                // count
                if let Some(neighbor_tile) = board.data[neighbor_loc.into()] {
                    let neighbor_tile: Tile = neighbor_tile;

                    // Find the city feature that matches the "center" one.
                    // `unwrap()` is called, since there's always a matching one.
                    let nei_matching_feat = neighbor_tile
                        .city_features()
                        .iter()
                        .find(|nei_city_feat| {
                            nei_city_feat
                                .iter()
                                .any(|nei_city_feat_side| nei_city_feat_side.opposite() == side)
                        })
                        .unwrap()
                        .clone();
                    for &nei_side in nei_matching_feat
                        .iter()
                        // TODO: What's this test for?
                        .filter(|side| **side != side.opposite())
                    {
                        stk.push((neighbor_loc, nei_side));
                    }
                } else {
                    *city_open_sides.get_mut(&new_city_id).unwrap() += 1;
                }
            } else {
                *city_open_sides.get_mut(&new_city_id).unwrap() += 1;
            }
        }
    }

    /// Returns the score winner of a city, and the raw score (without multiplier)
    /// Side effects:
    /// - Remove meeples belonging to this city from the board back to remaining_meeples
    fn calculate_city_score(
        city_id: NonZeroU8,
        city_ids: &Array2D<[Option<NonZeroU8>; 4]>,
        board: &mut Board,
        remaining_meeples: &mut [u8; 2],
        recycle_meeples: bool,
    ) -> (u16, [bool; 2]) {
        // 1. Determine who would get the score
        let mut meeple_counts = [0u16, 0];
        let is_player_scored = {
            for (x, y) in iproduct!(0..16, 0..16) {
                match &mut board[(x, y)] {
                    // Test if the tile exists and tile has a city meeple
                    Some(tile)
                        if matches!(
                            tile.meeple,
                            Some(Meeple {
                                kind: FeatureKind::City,
                                ..
                            })
                        ) =>
                    {
                        // Extract city feature index and color (player) from the meeple
                        let Meeple { index, player, .. } = tile.meeple.unwrap();
                        let ref city_feat = tile.city_features()[index];
                        let city_feat_side = city_feat[0];
                        if city_ids[(x, y)][city_feat_side.idx()] == Some(city_id) {
                            meeple_counts[player.idx()] += 1;

                            // Move the tile from the board back to remaining meeples
                            if recycle_meeples {
                                tile.meeple = None;
                                remaining_meeples[player.idx()] += 1;
                            }
                        }
                    }
                    _ => {}
                }
            }

            match meeple_counts[0].cmp(&meeple_counts[1]) {
                std::cmp::Ordering::Less => [false, true],
                std::cmp::Ordering::Equal if meeple_counts[0] == 0 => [false, false],
                std::cmp::Ordering::Equal => [true, true],
                std::cmp::Ordering::Greater => [true, false],
            }
        };

        // 2. Determine how much the score is
        let tile_count: u16 = iproduct!(0..16, 0..16)
            .map(|(x, y)| {
                let tile_has_this_city = Side::into_enum_iter()
                    .any(|side| city_ids[(x, y)][side.idx()] == Some(city_id));
                if tile_has_this_city {
                    board[(x, y)].unwrap().city_score()
                } else {
                    0
                }
            })
            .sum();

        (tile_count, is_player_scored)
    }

    /// Road floodfill algorithm. For all (loc, side) pairs connected to the road feature,
    ///
    /// - Update its corresponding `road_ids` entry
    /// - Return whether the road is closed upon construction. Return value is `true` iff the road
    /// is already closed.
    fn floodfill_road(
        road_ids: &mut Array2D<[Option<NonZeroU8>; 4]>,
        exist_roads: &mut HashSet<NonZeroU8>,
        board: &Board,
        from_tile: Tile,
        from_loc: Loc,
        from_feat_idx: usize,
        new_road_id: NonZeroU8,
    ) -> bool {
        let mut stk = vec![];
        let ref resident_feat = from_tile.road_features()[from_feat_idx];
        for &side in resident_feat.iter() {
            stk.push((from_loc, side));
        }

        let mut road_is_open = false;
        while let Some((loc, side)) = stk.pop() {
            let road_id = match &mut road_ids[loc.into()][side.idx()] {
                // Same road case
                Some(road_id) if *road_id == new_road_id => {
                    continue;
                }
                // Different road case
                other_id_or_none => other_id_or_none,
            };
            // Different road case
            if let Some(road_id) = road_id {
                exist_roads.remove(road_id);
            }
            *road_id = Some(new_road_id);

            if let Some(neighbor_loc) = loc.neighbor_on(side) {
                if let Some(neighbor_tile) = board.data[neighbor_loc.into()] {
                    let neighbor_tile: Tile = neighbor_tile;
                    let nei_matching_feat = neighbor_tile
                        .road_features()
                        .iter()
                        .find(|nei_road_feat| {
                            nei_road_feat
                                .iter()
                                .any(|nei_road_feat_side| nei_road_feat_side.opposite() == side)
                        })
                        .unwrap()
                        .clone();
                    for &nei_side in nei_matching_feat.iter()
                    // TODO: What's this test for?
                    // .filter(|side| **side != side.opposite())
                    {
                        stk.push((neighbor_loc, nei_side));
                    }
                } else {
                    road_is_open = true;
                }
            } else {
                road_is_open = true;
            }
        }

        !road_is_open
    }

    /// Returns the score winner of a road, and the raw score (without multiplier)
    /// Side effects:
    /// - Remove meeples belonging to this road from the board back to remaining_meeples
    fn calculate_road_score(
        road_id: NonZeroU8,
        road_ids: &Array2D<[Option<NonZeroU8>; 4]>,
        board: &mut Board,
        remaining_meeples: &mut [u8; 2],
        recycle_meeples: bool,
    ) -> (u16, [bool; 2]) {
        // 1. Determine who would get the score
        let mut meeple_counts = [0u16, 0];
        let is_player_scored = {
            for (x, y) in iproduct!(0..16, 0..16) {
                match &mut board[(x, y)] {
                    // Test if the tile exists and tile has a road meeple
                    Some(tile)
                        if matches!(
                            tile.meeple,
                            Some(Meeple {
                                kind: FeatureKind::Road,
                                ..
                            })
                        ) =>
                    {
                        // Extract road feature index and color (player) from the meeple
                        let Meeple { index, player, .. } = tile.meeple.unwrap();
                        let ref road_feat = tile.road_features()[index];
                        let road_feat_side = road_feat[0];
                        if road_ids[(x, y)][road_feat_side.idx()] == Some(road_id) {
                            meeple_counts[player.idx()] += 1;

                            // Move the tile from the board back to remaining meeples
                            if recycle_meeples {
                                tile.meeple = None;
                                remaining_meeples[player.idx()] += 1;
                            }
                        }
                    }
                    _ => {}
                }
            }

            match meeple_counts[0].cmp(&meeple_counts[1]) {
                std::cmp::Ordering::Less => [false, true],
                std::cmp::Ordering::Equal if meeple_counts[0] == 0 => [false, false],
                std::cmp::Ordering::Equal => [true, true],
                std::cmp::Ordering::Greater => [true, false],
            }
        };

        // 2. Determine how much the score is
        let tile_count: u16 = iproduct!(0..16, 0..16)
            .map(|(x, y)| {
                let tile_has_this_road = Side::into_enum_iter()
                    .any(|side| road_ids[(x, y)][side.idx()] == Some(road_id));
                if tile_has_this_road {
                    1
                } else {
                    0
                }
            })
            .sum();

        (tile_count, is_player_scored)
    }

    fn floodfill_field(
        field_ids: &mut Array2D<[Option<NonZeroU8>; 12]>,
        board: &Board,
        from_tile: Tile,
        from_loc: Loc,
        from_feat_idx: usize,
        new_field_id: NonZeroU8,
    ) {
        let mut stk = vec![];
        let ref resident_feat = from_tile.field_features()[from_feat_idx];

        // Initialize stack with (side, seg) pairs frfom that field feature
        for &(side, seg) in resident_feat.iter() {
            stk.push((from_loc, side, seg));
        }

        while let Some((loc, side, seg)) = stk.pop() {
            let field_id = match &mut field_ids[loc.into()][Self::side_seg_to_idx(side, seg)] {
                // Same field case
                Some(field_id) if *field_id == new_field_id => {
                    continue;
                }
                // Different field case
                other_id_or_none => other_id_or_none,
            };
            *field_id = Some(new_field_id);

            // Recurse (search down) if neighbor exists
            if let Some(neighbor_loc) = loc.neighbor_on(side) {
                if let Some(neighbor_tile) = board.data[neighbor_loc.into()] {
                    let neighbor_tile: Tile = neighbor_tile;
                    // For a neighbor to exist on the "field side", the neighbor must have a
                    // matching field feature as well.
                    let nei_matching_feat = neighbor_tile
                        .field_features()
                        .iter()
                        .find(|nei_field_feat| {
                            nei_field_feat.iter().any(
                                |&(nei_field_feat_side, nei_field_feat_seg)| {
                                    nei_field_feat_side == side.opposite()
                                        && nei_field_feat_seg == seg.opposite()
                                },
                            )
                        })
                        .cloned()
                        .unwrap();
                    // Recurse on all (loc, side, seg) tuples in the matching neighbor feature
                    for &(nei_side, nei_seg) in nei_matching_feat.iter() {
                        stk.push((neighbor_loc, nei_side, nei_seg));
                    }
                }
            }
        }
    }

    fn calculate_field_score(
        city_ids: &Array2D<[Option<NonZeroU8>; 4]>,
        complete_cities: &HashSet<NonZeroU8>,
        board: &Board,
        from_loc: Loc,
        from_feat_idx: usize,
    ) -> (u16, [bool; 2]) {
        let from_tile = board[Into::<(usize, usize)>::into(from_loc)].unwrap();

        let mut stk: Vec<(Loc, Side, Seg)> = vec![];
        let mut touched_city_ids = HashSet::new();
        let mut touched_positions: HashSet<(Loc, Side, Seg)> = HashSet::new();
        for &(side, seg) in from_tile.field_features()[from_feat_idx].iter() {
            stk.push((from_loc, side, seg));
        }

        let mut meeple_counts = [0u16, 0];
        while let Some((loc, side, seg)) = stk.pop() {
            if touched_positions.contains(&(loc, side, seg)) {
                continue;
            }

            let tile = board[Into::<(usize, usize)>::into(loc)].unwrap();
            let (containing_field_feat_idx, containing_field_feat) = tile
                .field_features()
                .iter()
                .cloned()
                .enumerate()
                .find(|(_, field_feature)| {
                    field_feature
                        .iter()
                        .any(|&(field_feat_side, field_feat_seg)| {
                            field_feat_side == side && field_feat_seg == seg
                        })
                })
                .unwrap();
            if let Tile {
                meeple:
                    Some(Meeple {
                        kind: FeatureKind::Field,
                        index,
                        player,
                        ..
                    }),
                ..
            } = tile
            {
                if index == containing_field_feat_idx {
                    meeple_counts[player.idx()] += 1;
                }
            }
            let adj_city_feat_indexes =
                tile.field_adj_city_feat_indexes()[containing_field_feat_idx];
            for &adj_city_feat_idx in adj_city_feat_indexes.iter() {
                for adj_city_side in tile.city_features()[adj_city_feat_idx].iter() {
                    if let Some(city_id) = city_ids[loc.into()][adj_city_side.idx()] {
                        // Only score those cities that have been completed
                        if complete_cities.contains(&city_id) {
                            touched_city_ids.insert(city_id);
                        }
                    }
                }
            }

            for &(field_side, field_seg) in containing_field_feat.iter() {
                touched_positions.insert((loc, field_side, field_seg));
                if let Some(neighbor_loc) = loc.neighbor_on(field_side) {
                    if let Some(_tile) = board.data[neighbor_loc.into()] {
                        // This tile must have a adjacent field feat
                        stk.push((neighbor_loc, field_side.opposite(), field_seg.opposite()));
                    }
                }
            }
        }

        let is_player_scored = match meeple_counts[0].cmp(&meeple_counts[1]) {
            std::cmp::Ordering::Less => [false, true],
            std::cmp::Ordering::Equal if meeple_counts[0] == 0 => [false, false],
            std::cmp::Ordering::Equal => [true, true],
            std::cmp::Ordering::Greater => [true, false],
        };
        let score = touched_city_ids.len() as u16;
        (score, is_player_scored)
    }

    /// Count surrounding tiles from a cloister location `location` and board reference `board`
    fn cloister_surround_count(location: Loc, board: &Board) -> u16 {
        let surround_count: u16 = location
            .surroundings_without_center()
            .map(
                |surround_loc| {
                    if board[surround_loc].is_some() {
                        1
                    } else {
                        0
                    }
                },
            )
            .sum();
        surround_count + 1
    }

    pub fn get_derivatives(&mut self) -> std::vec::IntoIter<(State, Action)> {
        let mut v = vec![];
        if let Some(drawn_tile) = self.drawn_tile {
            let actions = self.get_legal_actions_from_tile(drawn_tile);
            for action in actions.into_iter() {
                v.push((self.get_state_after_action(action), action));
            }
        } else {
            let decklen = self.deck.data.len();
            let mut state_action_buf: Vec<_> = (0..decklen)
                .into_par_iter()
                .map(|i| {
                    let mut state = self.clone();
                    // Pick out i'th tile from the deck
                    let raw_tile = state.deck.data.swap_remove(i);
                    // Get the actions
                    let actions = state.get_legal_actions_from_tile(raw_tile);
                    actions
                        .into_par_iter()
                        .map(move |action| (state.get_state_after_action(action), action))
                })
                .flatten()
                .collect();
            v.append(&mut state_action_buf);
        }
        v.into_iter()
    }

    pub fn get_state_after_action(&self, action: Action) -> State {
        let start = std::time::Instant::now();

        let mut board: Board = self.board.clone();
        let deck = self.deck.clone();
        let mut remaining_meeples = self.remaining_meeples.clone();
        /* city */
        let mut city_ids = self.city_ids.clone();
        let mut city_open_sides = self.city_open_sides.clone();
        let mut next_city_id = self.next_city_id.clone();
        let mut exist_cities = self.exist_cities.clone();
        let mut complete_cities = self.complete_cities.clone();
        /* road */
        let mut road_ids = self.road_ids.clone();
        let mut next_road_id = self.next_road_id;
        let mut exist_roads = self.exist_roads.clone();
        /* field */
        let mut field_ids = self.field_ids.clone();
        let mut next_field_id = self.next_field_id;
        /* misc */
        let next_player = self.next_player.opposite();
        let mut scores = self.scores.clone();
        let mut age = self.age;
        let last_action = Some(action);

        let actioning_player = self.next_player;
        // From now on, `self` should NOT be used at all

        let Action { location, tile, .. } = action;
        board[location] = Some(tile);

        // Shadow `action` to prevent further reference
        #[allow(unused_variables)]
        let action = ();

        // Meeple GENERATION step

        // placeholders for the propagation step later to skip the feature that's newly floodfilled
        let mut new_city_feat_idx = None;
        let mut new_road_feat_idx = None;
        let mut new_field_feat_idx = None;

        // generation stop runs only if there's a meeple
        if let Some(meeple) = tile.meeple {
            let feat_idx = meeple.index;
            // No matter the kind of feature it is, always subtract one from the meeple supply.
            remaining_meeples[actioning_player.idx()] -= 1;
            // TODO: Check recycling of meeples
            match meeple.kind {
                FeatureKind::Cloister => {
                    let surround_count = Self::cloister_surround_count(location, &board);
                    if surround_count == 9 {
                        // Cloister feature completed; remove meeple back
                        board[location].as_mut().unwrap().meeple = None;
                        remaining_meeples[actioning_player.idx()] += 1;

                        scores[actioning_player.idx()] += surround_count;
                    }
                }
                FeatureKind::Field => {
                    Self::floodfill_field(
                        &mut field_ids,
                        &board,
                        tile,
                        location,
                        feat_idx,
                        next_field_id,
                    );
                    new_field_feat_idx = Some(feat_idx);
                    next_field_id = NonZeroU8::new(next_field_id.get() + 1).unwrap();
                }
                FeatureKind::Road => {
                    let is_road_closed = Self::floodfill_road(
                        &mut road_ids,
                        &mut exist_roads,
                        &board,
                        tile,
                        location,
                        feat_idx,
                        next_road_id,
                    );

                    if is_road_closed {
                        // Fetch scores for this road
                        let (added_score, is_player_scored) = Self::calculate_road_score(
                            next_road_id,
                            &road_ids,
                            &mut board,
                            &mut remaining_meeples,
                            true,
                        );
                        // Add fetched scores to the players
                        for player in Player::into_enum_iter() {
                            if is_player_scored[player.idx()] {
                                scores[player.idx()] += added_score;
                            }
                        }
                    } else {
                        exist_roads.insert(next_road_id);
                    }
                    new_road_feat_idx = Some(feat_idx);
                    next_road_id = NonZeroU8::new(next_road_id.get() + 1).unwrap();
                }
                FeatureKind::City => {
                    // For the "yes meeple" case, a new city as always formed.
                    // Therefore, we can just proceed to create a new city (yay)

                    Self::floodfill_city(
                        &mut city_ids,
                        &mut city_open_sides,
                        &mut board,
                        &mut exist_cities,
                        tile,
                        location,
                        feat_idx,
                        next_city_id,
                    );

                    match city_open_sides.get(&next_city_id) {
                        // City is already closed upon creation
                        Some(open_sides) if *open_sides == 0 => {
                            // Fetch scores for this city
                            let (added_score, is_player_scored) = Self::calculate_city_score(
                                next_city_id,
                                &city_ids,
                                &mut board,
                                &mut remaining_meeples,
                                true,
                            );
                            // Add fetched scores to the players
                            for player in Player::into_enum_iter() {
                                if is_player_scored[player.idx()] {
                                    scores[player.idx()] += added_score * 2;
                                }
                            }

                            // Add to complete city list
                            complete_cities.insert(next_city_id);

                            // Remove the related entries
                            city_open_sides.remove(&next_city_id);
                        }
                        _ => {
                            exist_cities.insert(next_city_id);
                        }
                    }

                    new_city_feat_idx = Some(feat_idx);
                    next_city_id = NonZeroU8::new(next_city_id.get() + 1).unwrap();
                }
            }
        }

        // Meeple PROPAGATION step
        /* city propagation */
        for (feat_idx, city_feature) in tile.city_features().iter().enumerate() {
            // Skip the city that was just created
            if Some(feat_idx) == new_city_feat_idx {
                continue;
            }

            let adjacent_city_id = city_feature
                .iter()
                .filter_map(|&side| {
                    // Filter out those sides where adjacent city is not occupied
                    if let Some(neighbor_loc) = location.neighbor_on(side) {
                        if let Some(city_id) = city_ids[neighbor_loc.into()][side.opposite().idx()]
                        {
                            return Some(city_id);
                        }
                    }
                    None
                })
                .next();

            // Do a floodfill if there's adjacent city
            if let Some(_) = adjacent_city_id {
                // Replace (unite) the city (cities) with a new one
                Self::floodfill_city(
                    &mut city_ids,
                    &mut city_open_sides,
                    &mut board,
                    &mut exist_cities,
                    tile,
                    location,
                    feat_idx,
                    next_city_id,
                );

                match city_open_sides.get(&next_city_id) {
                    Some(open_sides) if *open_sides == 0 => {
                        // Fetch scores for this city
                        let (added_score, is_player_scored) = Self::calculate_city_score(
                            next_city_id,
                            &city_ids,
                            &mut board,
                            &mut remaining_meeples,
                            true,
                        );

                        // Add fetched scores to the players
                        for player in Player::into_enum_iter() {
                            if is_player_scored[player.idx()] {
                                scores[player.idx()] += added_score * 2;
                            }
                        }

                        complete_cities.insert(next_city_id);
                    }
                    _ => {
                        exist_cities.insert(next_city_id);
                    }
                }

                next_city_id = NonZeroU8::new(next_city_id.get() + 1).unwrap();
            }
        }

        /* road propagation */
        for (feat_idx, road_feature) in tile.road_features().iter().enumerate() {
            // Skip the road that was just created
            if Some(feat_idx) == new_road_feat_idx {
                continue;
            }

            let adjacent_road_id = road_feature
                .iter()
                .filter_map(|&side| {
                    // Filter out sides where adjacent road is not occupied.
                    // We're only interested in propagating adjacent occupied roads
                    if let Some(neighbor_loc) = location.neighbor_on(side) {
                        if let Some(road_id) = road_ids[neighbor_loc.into()][side.opposite().idx()]
                        {
                            return Some(road_id);
                        }
                    }
                    None
                })
                .next();

            if let Some(_) = adjacent_road_id {
                let is_road_closed = Self::floodfill_road(
                    &mut road_ids,
                    &mut exist_roads,
                    &board,
                    tile,
                    location,
                    feat_idx,
                    next_road_id,
                );
                if is_road_closed {
                    // Fetch scores for this road
                    let (added_score, is_player_scored) = Self::calculate_road_score(
                        next_road_id,
                        &road_ids,
                        &mut board,
                        &mut remaining_meeples,
                        true,
                    );
                    // Add fetched scores to the players
                    for player in Player::into_enum_iter() {
                        if is_player_scored[player.idx()] {
                            scores[player.idx()] += added_score;
                        }
                    }
                } else {
                    exist_roads.insert(next_road_id);
                }

                next_road_id = NonZeroU8::new(next_road_id.get() + 1).unwrap();
            }
        }

        /* field propagation */
        for (feat_idx, field_feature) in tile.field_features().iter().enumerate() {
            if Some(feat_idx) == new_field_feat_idx {
                continue;
            }

            let adjacent_field_id = field_feature
                .iter()
                .filter_map(|&(feat_side, feat_seg)| {
                    if let Some(neighbor_loc) = location.neighbor_on(feat_side) {
                        if let Some(field_id) = field_ids[neighbor_loc.into()]
                            [Self::side_seg_to_idx(feat_side.opposite(), feat_seg.opposite())]
                        {
                            return Some(field_id);
                        }
                    }
                    None
                })
                .next();

            if let Some(_) = adjacent_field_id {
                Self::floodfill_field(
                    &mut field_ids,
                    &board,
                    tile,
                    location,
                    feat_idx,
                    next_field_id,
                );
                next_field_id = NonZeroU8::new(next_field_id.get() + 1).unwrap();
            }
        }

        /* cloister propagation */
        for surround_loc in location.surroundings_without_center() {
            // Only match tiles with both a) with cloisters and b) meeple on the cloister
            match board[surround_loc] {
                Some(tile) if tile.has_cloister() => match tile.meeple {
                    Some(Meeple {
                        kind: FeatureKind::Cloister,
                        player,
                        ..
                    }) => {
                        let surround_count = Self::cloister_surround_count(surround_loc, &board);
                        if surround_count == 9 {
                            // Cloister feature completed; remove meeple back
                            board[surround_loc].as_mut().unwrap().meeple = None;
                            remaining_meeples[player.idx()] += 1;

                            scores[player.idx()] += surround_count;
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        // End score step
        age += 1;
        if age == 71 {
            /* city final scoring */
            for &city_id in exist_cities.iter() {
                let open_sides = *city_open_sides.get(&city_id).unwrap();
                // Already-closed cities are already scored
                if open_sides == 0 {
                    continue;
                }

                // Fetch scores for this city
                let (added_score, is_player_scored) = Self::calculate_city_score(
                    city_id,
                    &city_ids,
                    &mut board,
                    &mut remaining_meeples,
                    false,
                );

                // Add fetched scores to the players
                for player in Player::into_enum_iter() {
                    if is_player_scored[player.idx()] {
                        scores[player.idx()] += added_score;
                    }
                }
            }

            /* road final scoring */
            for &road_id in exist_roads.iter() {
                // Fetch scores
                let (added_score, is_player_scored) = Self::calculate_road_score(
                    road_id,
                    &road_ids,
                    &mut board,
                    &mut remaining_meeples,
                    false,
                );

                for player in Player::into_enum_iter() {
                    if is_player_scored[player.idx()] {
                        scores[player.idx()] += added_score;
                    }
                }
            }

            /* field final scoring */
            let mut accounted_field_ids = HashSet::new();
            for (x, y) in iproduct!(0..16, 0..16) {
                if let Some(tile) = board[(x, y)] {
                    if let Some(Meeple {
                        kind: FeatureKind::Field,
                        index,
                        ..
                    }) = tile.meeple
                    {
                        let (field_side, field_seg) = tile.field_features()[index][0];
                        if let Some(field_id) =
                            field_ids[(x, y)][Self::side_seg_to_idx(field_side, field_seg)]
                        {
                            if !accounted_field_ids.contains(&field_id) {
                                let (added_score, is_player_scored) = Self::calculate_field_score(
                                    &city_ids,
                                    &complete_cities,
                                    &board,
                                    Loc::new(x as i32, y as i32),
                                    index,
                                );
                                for player in Player::into_enum_iter() {
                                    if is_player_scored[player.idx()] {
                                        scores[player.idx()] += added_score * 3;
                                    }
                                }
                                accounted_field_ids.insert(field_id);
                            }
                        }
                    }
                }
            }

            /* cloister final scoring */
            for (x, y) in iproduct!(0..16, 0..16) {
                let center_loc = Loc::new(x, y);
                match board[center_loc] {
                    Some(Tile {
                        meeple:
                            Some(Meeple {
                                kind: FeatureKind::Cloister,
                                player,
                                ..
                            }),
                        ..
                    }) => {
                        let surround_count = Self::cloister_surround_count(center_loc, &board);
                        scores[player.idx()] += surround_count;
                    }
                    _ => {
                        continue;
                    }
                };
            }
        }

        let end = std::time::Instant::now();
        DER_ACTION_TIMES.lock().unwrap().push(end - start);

        Self {
            board,
            deck,
            remaining_meeples,
            city_ids,
            city_open_sides,
            next_city_id,
            exist_cities,
            complete_cities,
            road_ids,
            next_road_id,
            exist_roads,
            next_player,
            scores,
            age,
            field_ids,
            next_field_id,
            last_action,
            drawn_tile: None,
        }
    }

    pub fn draw_and_get_legal_actions(&mut self) -> Vec<Action> {
        assert!(self.draw_tile());
        self.get_legal_actions_from_tile(self.drawn_tile.unwrap())
    }

    pub fn undraw_tile(&mut self) {
        self.put_back_tile(self.drawn_tile.unwrap());
        self.drawn_tile = None;
    }

    pub fn get_legal_actions_from_tile(&self, raw_tile_drawn: RawTile) -> Vec<Action> {
        let start = std::time::Instant::now();

        let mut avail_locations = HashSet::new();
        // For each center location that's occupied
        for (x, y) in iproduct!(0..16, 0..16) {
            let center_loc: Loc = (x, y).into();
            if matches!(self.board[center_loc], None) {
                continue;
            }

            // For each side of the center location, add its empty neighbors to avail_locations
            for side in Side::into_enum_iter() {
                if let Some(neighbor_loc) = center_loc.neighbor_on(side) {
                    if matches!(self.board[neighbor_loc], None) {
                        avail_locations.insert(neighbor_loc);
                    }
                }
            }
        }

        let mut avail_actions: Vec<Action> = vec![];

        // For each available center location (empty)
        for loc in avail_locations.into_iter() {
            // For each way of rotation
            for &rotation in raw_tile_drawn.allowed_rotations() {
                // Get rotated form of the center (drawn) tile
                let center_raw_tile = raw_tile_drawn;
                let center_tile = Tile::new(center_raw_tile, rotation, None);

                // Test if all sides match with adjacent tiles
                let is_all_sides_match = Side::into_enum_iter().all(|side| {
                    // Only test if neighbor location is valid
                    if let Some(neighbor_loc) = loc.neighbor_on(side) {
                        // Only test if neighbor tile is not empty
                        if let Some(neighbor_tile) = self.board[neighbor_loc] {
                            // Get the two borders
                            let center_side_border = center_tile.border_at(side);
                            let neighbor_opposite_side_border =
                                neighbor_tile.border_at(side.opposite());
                            // Return "not matching (false)" if they don't match
                            if center_side_border != neighbor_opposite_side_border {
                                return false;
                            }
                        }
                    }

                    return true;
                });

                if is_all_sides_match {
                    avail_actions.push(Action::new(loc, center_tile));
                }
            }
        }

        // Add actions with meeples if there's still some remaining meeple
        if self.remaining_meeples[self.next_player.idx()] > 0 {
            let mut avail_actions_with_meeple: Vec<Action> = vec![];

            for action in avail_actions.iter() {
                /* City meeple placement */

                // For each feature position
                for (feat_idx, city_feature) in action.tile.city_features().iter().enumerate() {
                    // If the feature is adjacent to an occupied area, then it's not legal to place a
                    // new meeple anymore.
                    // To check this in a efficient manner, we should store "whether a side of a tile
                    // is a side of occupied city".
                    // This should be accomplished by a 16x16 grid, with each cell containing 4 cells
                    // of bool information.
                    let is_legal = city_feature.iter().all(|&city_side| {
                        // Only do the negative check if `action.location` not on the edge of the map
                        if let Some(neighbor_loc) = action.location.neighbor_on(city_side) {
                            if let Some(_city_id) =
                                self.city_ids[neighbor_loc.into()][city_side.opposite().idx()]
                            {
                                return false;
                            }
                        }

                        true
                    });

                    if is_legal {
                        let tile_with_meeple = action.tile.with_meeple(Meeple::new(
                            FeatureKind::City,
                            feat_idx,
                            self.next_player,
                        ));
                        let action_with_meeple = Action::new(action.location, tile_with_meeple);
                        avail_actions_with_meeple.push(action_with_meeple);
                    }
                }

                /* Road meeple placement */
                for (feat_idx, road_feature) in action.tile.road_features().iter().enumerate() {
                    let is_legal = road_feature.iter().all(|&road_side| {
                        if let Some(neighbor_loc) = action.location.neighbor_on(road_side) {
                            if let Some(_road_id) =
                                self.road_ids[neighbor_loc.into()][road_side.opposite().idx()]
                            {
                                return false;
                            }
                        }
                        true
                    });

                    if is_legal {
                        let tile_with_meeple = action.tile.with_meeple(Meeple::new(
                            FeatureKind::Road,
                            feat_idx,
                            self.next_player,
                        ));
                        let action_with_meeple = Action::new(action.location, tile_with_meeple);
                        avail_actions_with_meeple.push(action_with_meeple);
                    }
                }

                /* Field meeple placement */
                for (feat_idx, field_feature) in action.tile.field_features().iter().enumerate() {
                    let is_legal = field_feature.iter().all(|&(field_side, field_seg)| {
                        if let Some(neighbor_loc) = action.location.neighbor_on(field_side) {
                            if let Some(_field_id) = self.field_ids[neighbor_loc.into()]
                                [Self::side_seg_to_idx(field_side.opposite(), field_seg.opposite())]
                            {
                                return false;
                            }
                        }
                        true
                    });

                    if is_legal {
                        let tile_with_meeple = action.tile.with_meeple(Meeple::new(
                            FeatureKind::Field,
                            feat_idx,
                            self.next_player,
                        ));
                        let action_with_meeple = Action::new(action.location, tile_with_meeple);
                        avail_actions_with_meeple.push(action_with_meeple);
                    }
                }

                /* Cloister meeple placement */
                if action.tile.has_cloister() {
                    let tile_with_meeple = action.tile.with_meeple(Meeple::new(
                        FeatureKind::Cloister,
                        0,
                        self.next_player,
                    ));
                    let action_with_meeple = Action::new(action.location, tile_with_meeple);
                    avail_actions_with_meeple.push(action_with_meeple);
                }
            }

            // End of meeple tests, merge (push) back to the main vector of `avail_actions`
            avail_actions.append(&mut avail_actions_with_meeple);
        }

        let end = std::time::Instant::now();
        GET_ACTION_TIMES.lock().unwrap().push(end - start);

        avail_actions
    }

    fn side_seg_to_idx(side: Side, seg: Seg) -> usize {
        side.idx() * 3 + seg.idx()
    }
}

impl AsImg for State {
    fn dyn_img(&self) -> Result<image::DynamicImage, Box<dyn std::error::Error>> {
        let mut board_img = self.board.dyn_img()?;
        let debug_yellow_img = DebugYellowImg.dyn_img()?;
        let debug_pink_img = DebugPinkImg.dyn_img()?;
        let debug_blue_img = DebugBlueImg.dyn_img()?;
        let debug_orange_img = DebugOrangeImg.dyn_img()?;
        let debug_maru_grey_img = DebugMaruGreyImg.dyn_img()?;

        for (x, y) in iproduct!(0..16, 0..16) {
            let bx = (x * 64) as u32;
            let by = (y * 64) as u32;
            for side in Side::into_enum_iter() {
                if let Some(city_id) = self.city_ids[(x, y)][side.idx()] {
                    let (sx, sy) = match side {
                        Side::North => (24, 0),
                        Side::East => (48, 24),
                        Side::South => (24, 48),
                        Side::West => (0, 24),
                    };
                    if let Some(0) | None = self.city_open_sides.get(&city_id) {
                        image::imageops::overlay(&mut board_img, &debug_pink_img, bx + sx, by + sy);
                    } else {
                        image::imageops::overlay(
                            &mut board_img,
                            &debug_yellow_img,
                            bx + sx,
                            by + sy,
                        );
                    }
                }

                if let Some(road_id) = self.road_ids[(x, y)][side.idx()] {
                    let (sx, sy) = match side {
                        Side::North => (24, 0),
                        Side::East => (48, 24),
                        Side::South => (24, 48),
                        Side::West => (0, 24),
                    };
                    if self.exist_roads.contains(&road_id) {
                        image::imageops::overlay(&mut board_img, &debug_blue_img, bx + sx, by + sy);
                    } else {
                        image::imageops::overlay(
                            &mut board_img,
                            &debug_orange_img,
                            bx + sx,
                            by + sy,
                        );
                    }
                }

                for seg in Seg::into_enum_iter() {
                    if let Some(_field_id) =
                        self.field_ids[(x, y)][Self::side_seg_to_idx(side, seg)]
                    {
                        let (sx, sy) = match (side, seg) {
                            (Side::North, Seg::First) => (8, 0),
                            (Side::North, Seg::Mid) => (24, 0),
                            (Side::North, Seg::Last) => (40, 0),
                            (Side::East, Seg::First) => (48, 8),
                            (Side::East, Seg::Mid) => (48, 24),
                            (Side::East, Seg::Last) => (48, 40),
                            (Side::South, Seg::First) => (40, 48),
                            (Side::South, Seg::Mid) => (24, 48),
                            (Side::South, Seg::Last) => (8, 48),
                            (Side::West, Seg::First) => (0, 40),
                            (Side::West, Seg::Mid) => (0, 24),
                            (Side::West, Seg::Last) => (0, 8),
                        };
                        image::imageops::overlay(
                            &mut board_img,
                            &debug_maru_grey_img,
                            bx + sx,
                            by + sy,
                        );
                    }
                }
            }
        }

        const FONT_DATA: &[u8] = include_bytes!("../resources/misc/FiraMono-Regular.ttf");
        let font: rusttype::Font<'static> = rusttype::Font::try_from_bytes(FONT_DATA).unwrap();

        imageproc::drawing::draw_text_mut(
            &mut board_img,
            image::Rgba([255, 255, 255, 255]),
            0,
            0,
            rusttype::Scale { x: 32., y: 32. },
            &font,
            &format!("R {} : {} B", self.scores[0], self.scores[1]),
        );

        Ok(board_img)
    }
}

/// Macro to create impl's for debug images
macro_rules! DebugAsImg {
    ($name: ty, $fname: expr) => {
        impl AsImg for $name {
            fn img_bytes(&self) -> &'static [u8] {
                include_bytes!(concat!("../resources/misc/", $fname, ".png"))
            }
        }
    };
}

struct DebugYellowImg;
DebugAsImg!(DebugYellowImg, "DebugYellow");

struct DebugPinkImg;
DebugAsImg!(DebugPinkImg, "DebugPink");

struct DebugBlueImg;
DebugAsImg!(DebugBlueImg, "DebugBlue");

struct DebugOrangeImg;
DebugAsImg!(DebugOrangeImg, "DebugOrange");

struct DebugGreyImg;
DebugAsImg!(DebugGreyImg, "DebugGrey");

struct DebugMaruGreyImg;
DebugAsImg!(DebugMaruGreyImg, "DebugMaruGrey");

#[derive(Debug, Clone, Copy)]
pub struct Action {
    location: Loc,
    tile: Tile,
}

impl Action {
    fn new(location: Loc, tile: Tile) -> Self {
        Self { location, tile }
    }

    /// For serialization
    pub fn get_location(&self) -> (usize, usize) {
        self.location.into()
    }

    /// For serialization
    pub fn get_tile_idx(&self) -> usize {
        match self.tile.meeple {
            None => 0,
            Some(Meeple {
                kind: FeatureKind::City,
                index,
                ..
            }) => (index as usize) + 1,
            Some(Meeple {
                kind: FeatureKind::Road,
                index,
                ..
            }) => (index as usize) + 5,
            Some(Meeple {
                kind: FeatureKind::Field,
                index,
                ..
            }) => (index as usize) + 9,
            Some(Meeple {
                kind: FeatureKind::Cloister,
                ..
            }) => 13,
        }
    }
}

#[derive(Debug, Clone)]
struct Board {
    data: Array2D<Option<Tile>>,
}

impl Board {
    /// Create new board with the starting tile at (7, 7) with random rotation
    fn new() -> Self {
        let mut board = Self {
            data: Array2D::filled_with(None, 16, 16),
        };
        board.data[(7, 7)] = Some(Tile::new(
            RawTile::CastleWallRoad,
            Rotation::into_enum_iter()
                .choose(&mut thread_rng())
                .unwrap(),
            None,
        ));
        board
    }
}

impl std::ops::Index<(usize, usize)> for Board {
    type Output = Option<Tile>;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        &self.data[index]
    }
}

impl std::ops::IndexMut<(usize, usize)> for Board {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        &mut self.data[index]
    }
}

impl std::ops::Index<Loc> for Board {
    type Output = Option<Tile>;

    fn index(&self, index: Loc) -> &Self::Output {
        &self.data[(index.x as usize, index.y as usize)]
    }
}

impl std::ops::IndexMut<Loc> for Board {
    fn index_mut(&mut self, index: Loc) -> &mut Self::Output {
        &mut self.data[(index.x as usize, index.y as usize)]
    }
}

impl AsImg for Board {
    fn dyn_img(&self) -> Result<image::DynamicImage, Box<dyn std::error::Error>> {
        let mut canvas = image::DynamicImage::new_rgba8(1024, 1024);
        for (x, y) in iproduct!(0..16, 0..16) {
            if let Some(tile) = self[(x, y)] {
                let tile_img = tile.dyn_img()?;
                image::imageops::overlay(&mut canvas, &tile_img, (x * 64) as u32, (y * 64) as u32);
            }
        }
        Ok(canvas)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Loc {
    x: i32,
    y: i32,
}

impl Loc {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    fn neighbor_on(&self, side: Side) -> Option<Self> {
        let (sx, sy) = side.to_shift();
        let (x, y) = (self.x + sx, self.y + sy);
        if x < 0 || x >= BOARD_SIZE as i32 || y < 0 || y >= BOARD_SIZE as i32 {
            None
        } else {
            Some(Loc::new(x, y))
        }
    }

    fn surroundings_without_center(&self) -> impl Iterator<Item = Self> {
        let loc = self.clone();
        iproduct!(-1..=1, -1..=1)
            .filter(|&(x, y)| !(x == 0 && y == 0))
            .filter_map(move |(x, y)| loc + (x, y))
    }
}

impl std::ops::Add<(i32, i32)> for Loc {
    type Output = Option<Loc>;

    fn add(self, (sx, sy): (i32, i32)) -> Self::Output {
        let (x, y) = (self.x + sx, self.y + sy);
        if x < 0 || x >= BOARD_SIZE as i32 || y < 0 || y >= BOARD_SIZE as i32 {
            None
        } else {
            Some(Loc::new(x, y))
        }
    }
}

impl From<(usize, usize)> for Loc {
    fn from((x, y): (usize, usize)) -> Self {
        Self {
            x: x as i32,
            y: y as i32,
        }
    }
}

impl Into<(usize, usize)> for Loc {
    fn into(self) -> (usize, usize) {
        (self.x as usize, self.y as usize)
    }
}

#[derive(Debug, Clone)]
struct Deck {
    data: Vec<RawTile>,
}

impl Deck {
    /// Create new deck with a starting tile missing
    fn new() -> Self {
        // Use vector first because `shuffle` only works on slices
        let mut data = vec![];
        data.reserve(72);

        for raw_tile in RawTile::into_enum_iter() {
            for _ in 0..raw_tile.count_without_starting_tile() {
                data.push(raw_tile);
            }
        }

        Self { data }
    }

    /// Draw a raw tile from the deck at random. Note that this doesn't take whether the drawn tile
    /// is placable or not into account, and the caller must check (and maybe put the tile back) by
    /// itself.
    fn draw(&mut self) -> Option<RawTile> {
        if self.data.is_empty() {
            None
        } else {
            let index = thread_rng().gen_range(0..self.data.len());
            Some(self.data.swap_remove(index))
        }
    }

    fn draw_at(&mut self, index: usize) -> RawTile {
        self.data.swap_remove(index)
    }

    fn put_back(&mut self, raw_tile: RawTile) {
        self.data.push(raw_tile);
    }
}

#[derive(Debug, Clone, Copy)]
pub(in crate) struct Tile {
    raw_tile: RawTile,
    rotation: Rotation,
    meeple: Option<Meeple>,
}

impl Tile {
    fn new(raw_tile: RawTile, rotation: Rotation, meeple: Option<Meeple>) -> Self {
        Self {
            raw_tile,
            rotation,
            meeple,
        }
    }

    fn with_meeple(mut self, meeple: Meeple) -> Self {
        self.meeple = Some(meeple);
        self
    }

    fn border_at(&self, side: Side) -> Border {
        self.raw_tile.borders()[side.unrotate_by(self.rotation).idx()]
    }

    /// Get an array of city features. Each city feature is represented by an array of sides.
    fn city_features(&self) -> ArrayVec<[ArrayVec<[Side; 4]>; 2]> {
        self.raw_tile
            .city_features()
            .iter()
            .map(|feat| {
                feat.iter()
                    .map(|side| side.rotate_by(self.rotation))
                    .collect()
            })
            .collect()
    }

    /// Get an array of field features. Each field feature is represented by an array of
    /// `(Side, Seg)` pairs.
    fn field_features(&self) -> ArrayVec<[ArrayVec<[(Side, Seg); 12]>; 4]> {
        self.raw_tile
            .field_features()
            .iter()
            .map(|feat| {
                feat.iter()
                    .map(|(side, seg)| (side.rotate_by(self.rotation), *seg))
                    .collect()
            })
            .collect()
    }

    /// Get an array of road features. Each road feature is represented by an array of sides.
    fn road_features(&self) -> ArrayVec<[ArrayVec<[Side; 2]>; 4]> {
        self.raw_tile
            .road_features()
            .iter()
            .map(|feat| {
                feat.iter()
                    .map(|side| side.rotate_by(self.rotation))
                    .collect()
            })
            .collect()
    }

    fn has_cloister(&self) -> bool {
        self.raw_tile.has_cloister()
    }

    fn city_score(&self) -> u16 {
        self.raw_tile.city_score()
    }

    fn field_adj_city_feat_indexes(&self) -> &'static [&'static [usize]] {
        self.raw_tile.field_adj_city_feat_indexes()
    }
}

impl AsImg for Tile {
    fn dyn_img(&self) -> Result<image::DynamicImage, Box<dyn std::error::Error>> {
        let raw_img = self.raw_tile.dyn_img()?;
        let mut rotated_img = match self.rotation {
            Rotation::Zero => raw_img,
            Rotation::One => raw_img.rotate90(),
            Rotation::Two => raw_img.rotate180(),
            Rotation::Three => raw_img.rotate270(),
        };
        if let Some(meeple) = self.meeple {
            let (x, y) = match meeple.kind {
                FeatureKind::Field => {
                    let (meeple_side, meeple_seg) = self.field_features()[meeple.index][0];
                    let (x, y) = match (meeple_side, meeple_seg) {
                        (Side::North, Seg::First) => (8, 0),
                        (Side::North, Seg::Mid) => (24, 0),
                        (Side::North, Seg::Last) => (40, 0),
                        (Side::East, Seg::First) => (48, 8),
                        (Side::East, Seg::Mid) => (48, 24),
                        (Side::East, Seg::Last) => (48, 40),
                        (Side::South, Seg::First) => (40, 48),
                        (Side::South, Seg::Mid) => (24, 48),
                        (Side::South, Seg::Last) => (8, 48),
                        (Side::West, Seg::First) => (0, 40),
                        (Side::West, Seg::Mid) => (0, 24),
                        (Side::West, Seg::Last) => (0, 8),
                    };
                    (x, y)
                }
                FeatureKind::Cloister => (24, 24),
                _ => {
                    let meeple_side = match meeple.kind {
                        FeatureKind::Road => self.road_features()[meeple.index][0],
                        FeatureKind::City => self.city_features()[meeple.index][0],
                        _ => panic!("impossible branch"),
                    };
                    let (x, y) = match meeple_side {
                        Side::North => (24, 0),
                        Side::East => (48, 24),
                        Side::South => (24, 48),
                        Side::West => (0, 24),
                    };
                    (x, y)
                }
            };
            let meeple_img = meeple.dyn_img()?;
            image::imageops::overlay(&mut rotated_img, &meeple_img, x, y);
        }
        Ok(rotated_img)
    }
}

#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq)]
#[repr(u8)]
pub enum RawTile {
    CastleCenterShield = 1,
    CastleCenterEntry = 2,
    CastleCenterSide = 3,
    CastleEdge = 4,
    CastleEdgeRoad = 5,
    CastleSides = 6,
    CastleSidesEdge = 7,
    CastleTube = 8,
    CastleWall = 9,
    CastleWallCurveLeft = 10,
    CastleWallCurveRight = 11,
    CastleWallJunction = 12,
    CastleWallRoad = 13,
    Cloister = 14,
    CloisterRoad = 15,
    Road = 16,
    RoadCurve = 17,
    RoadJunctionLarge = 18,
    RoadJunctionSmall = 19,
    CastleCenterEntryShield = 20,
    CastleCenterSideShield = 21,
    CastleEdgeShield = 22,
    CastleEdgeRoadShield = 23,
    CastleTubeShield = 24,
}

impl RawTile {
    fn borders(&self) -> [Border; 4] {
        const C: Border = Border::City;
        const R: Border = Border::Road;
        const F: Border = Border::Field;

        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => [C, C, C, C],
            RawTile::CastleCenterEntry       => [C, C, R, C],
            RawTile::CastleCenterSide        => [C, C, F, C],
            RawTile::CastleEdge              => [C, C, F, F],
            RawTile::CastleEdgeRoad          => [C, C, R, R],
            RawTile::CastleSides             => [C, F, C, F],
            RawTile::CastleSidesEdge         => [C, F, F, C],
            RawTile::CastleTube              => [F, C, F, C],
            RawTile::CastleWall              => [C, F, F, F],
            RawTile::CastleWallCurveLeft     => [C, F, R, R],
            RawTile::CastleWallCurveRight    => [C, R, R, F],
            RawTile::CastleWallJunction      => [C, R, R, R],
            RawTile::CastleWallRoad          => [C, R, F, R],
            RawTile::Cloister                => [F, F, F, F],
            RawTile::CloisterRoad            => [F, F, R, F],
            RawTile::Road                    => [R, F, R, F],
            RawTile::RoadCurve               => [F, F, R, R],
            RawTile::RoadJunctionLarge       => [R, R, R, R],
            RawTile::RoadJunctionSmall       => [F, R, R, R],
            RawTile::CastleCenterEntryShield => [C, C, R, C],
            RawTile::CastleCenterSideShield  => [C, C, F, C],
            RawTile::CastleEdgeShield        => [C, C, F, F],
            RawTile::CastleEdgeRoadShield    => [C, C, R, R],
            RawTile::CastleTubeShield        => [F, C, F, C],
        }
    }

    fn city_features(&self) -> &'static [&'static [Side]] {
        use Side::*;

        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => &[&[North, East, South, West]],
            RawTile::CastleCenterEntry       => &[&[North, East, West]],
            RawTile::CastleCenterSide        => &[&[North, East, West]],
            RawTile::CastleEdge              => &[&[North, East]],
            RawTile::CastleEdgeRoad          => &[&[North, East]],
            RawTile::CastleSides             => &[&[North], &[South]],
            RawTile::CastleSidesEdge         => &[&[North], &[West]],
            RawTile::CastleTube              => &[&[East, West]],
            RawTile::CastleWall              => &[&[North]],
            RawTile::CastleWallCurveLeft     => &[&[North]],
            RawTile::CastleWallCurveRight    => &[&[North]],
            RawTile::CastleWallJunction      => &[&[North]],
            RawTile::CastleWallRoad          => &[&[North]],
            RawTile::Cloister                => &[],
            RawTile::CloisterRoad            => &[],
            RawTile::Road                    => &[],
            RawTile::RoadCurve               => &[],
            RawTile::RoadJunctionLarge       => &[],
            RawTile::RoadJunctionSmall       => &[],
            RawTile::CastleCenterEntryShield => &[&[North, East, West]],
            RawTile::CastleCenterSideShield  => &[&[North, East, West]],
            RawTile::CastleEdgeShield        => &[&[North, East]],
            RawTile::CastleEdgeRoadShield    => &[&[North, East]],
            RawTile::CastleTubeShield        => &[&[East, West]],
        }
    }

    fn field_features(&self) -> &'static [&'static [(Side, Seg)]] {
        use Seg::*;
        use Side::*;

        match self {
            RawTile::CastleCenterShield => &[],
            RawTile::CastleCenterEntry => &[&[(South, First)], &[(South, Last)]],
            RawTile::CastleCenterSide => &[&[(South, First), (South, Mid), (South, Last)]],
            RawTile::CastleEdge => &[&[
                (South, First),
                (South, Mid),
                (South, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::CastleEdgeRoad => &[
                &[(South, First), (West, Last)],
                &[(South, Last), (West, First)],
            ],
            RawTile::CastleSides => &[&[
                (East, First),
                (East, Mid),
                (East, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::CastleSidesEdge => &[&[
                (East, First),
                (East, Mid),
                (East, Last),
                (South, First),
                (South, Mid),
                (South, Last),
            ]],
            RawTile::CastleTube => &[
                &[(North, First), (North, Mid), (North, Last)],
                &[(South, First), (South, Mid), (South, Last)],
            ],
            RawTile::CastleWall => &[&[
                (East, First),
                (East, Mid),
                (East, Last),
                (South, First),
                (South, Mid),
                (South, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::CastleWallCurveLeft => &[
                &[
                    (East, First),
                    (East, Mid),
                    (East, Last),
                    (South, First),
                    (West, Last),
                ],
                &[(South, Last), (West, First)],
            ],
            RawTile::CastleWallCurveRight => &[
                &[
                    (East, First),
                    (South, Last),
                    (West, First),
                    (West, Mid),
                    (West, Last),
                ],
                &[(East, Last), (South, First)],
            ],
            RawTile::CastleWallJunction => &[
                &[(East, First), (West, Last)],
                &[(East, Last), (South, First)],
                &[(South, Last), (West, First)],
            ],
            RawTile::CastleWallRoad => &[
                &[(East, First), (West, Last)],
                &[
                    (East, Last),
                    (South, First),
                    (South, Mid),
                    (South, Last),
                    (West, First),
                ],
            ],
            RawTile::Cloister => &[&[
                (North, First),
                (North, Mid),
                (North, Last),
                (East, First),
                (East, Mid),
                (East, Last),
                (South, First),
                (South, Mid),
                (South, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::CloisterRoad => &[&[
                (North, First),
                (North, Mid),
                (North, Last),
                (East, First),
                (East, Mid),
                (East, Last),
                (South, First),
                (South, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::Road => &[
                &[
                    (North, Last),
                    (East, First),
                    (East, Mid),
                    (East, Last),
                    (South, First),
                ],
                &[
                    (South, Last),
                    (West, First),
                    (West, Mid),
                    (West, Last),
                    (North, First),
                ],
            ],
            RawTile::RoadCurve => &[
                &[
                    (North, First),
                    (North, Mid),
                    (North, Last),
                    (East, First),
                    (East, Mid),
                    (East, Last),
                    (South, First),
                    (West, Last),
                ],
                &[(South, Last), (West, First)],
            ],
            RawTile::RoadJunctionLarge => &[
                &[(North, Last), (East, First)],
                &[(East, Last), (South, First)],
                &[(South, Last), (West, First)],
                &[(West, Last), (North, First)],
            ],
            RawTile::RoadJunctionSmall => &[
                &[
                    (North, First),
                    (North, Mid),
                    (North, Last),
                    (East, First),
                    (West, Last),
                ],
                &[(East, Last), (South, First)],
                &[(South, Last), (West, First)],
            ],
            RawTile::CastleCenterEntryShield => &[&[(South, First)], &[(South, Last)]],
            RawTile::CastleCenterSideShield => &[&[(South, First), (South, Mid), (South, Last)]],
            RawTile::CastleEdgeShield => &[&[
                (South, First),
                (South, Mid),
                (South, Last),
                (West, First),
                (West, Mid),
                (West, Last),
            ]],
            RawTile::CastleEdgeRoadShield => &[
                &[(South, First), (West, Last)],
                &[(South, Last), (West, First)],
            ],
            RawTile::CastleTubeShield => &[
                &[(North, First), (North, Mid), (North, Last)],
                &[(South, First), (South, Mid), (South, Last)],
            ],
        }
    }

    fn road_features(&self) -> &'static [&'static [Side]] {
        use Side::*;

        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => &[],
            RawTile::CastleCenterEntry       => &[&[South]],
            RawTile::CastleCenterSide        => &[],
            RawTile::CastleEdge              => &[],
            RawTile::CastleEdgeRoad          => &[&[South, West]],
            RawTile::CastleSides             => &[],
            RawTile::CastleSidesEdge         => &[],
            RawTile::CastleTube              => &[],
            RawTile::CastleWall              => &[],
            RawTile::CastleWallCurveLeft     => &[&[South, West]],
            RawTile::CastleWallCurveRight    => &[&[East, South]],
            RawTile::CastleWallJunction      => &[&[East], &[South], &[West]],
            RawTile::CastleWallRoad          => &[&[East, West]],
            RawTile::Cloister                => &[],
            RawTile::CloisterRoad            => &[&[South]],
            RawTile::Road                    => &[&[North, South]],
            RawTile::RoadCurve               => &[&[South, West]],
            RawTile::RoadJunctionLarge       => &[&[North], &[East], &[South], &[West]],
            RawTile::RoadJunctionSmall       => &[&[East], &[South], &[West]],
            RawTile::CastleCenterEntryShield => &[&[South]],
            RawTile::CastleCenterSideShield  => &[],
            RawTile::CastleEdgeShield        => &[],
            RawTile::CastleEdgeRoadShield    => &[&[South, West]],
            RawTile::CastleTubeShield        => &[],
        }
    }

    fn field_adj_city_feat_indexes(&self) -> &'static [&'static [usize]] {
        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => &[],
            RawTile::CastleCenterEntry       => &[&[0], &[0]],
            RawTile::CastleCenterSide        => &[&[0]],
            RawTile::CastleEdge              => &[&[0]],
            RawTile::CastleEdgeRoad          => &[&[0], &[]],
            RawTile::CastleSides             => &[&[0, 1]],
            RawTile::CastleSidesEdge         => &[&[0, 1]],
            RawTile::CastleTube              => &[&[0], &[0]],
            RawTile::CastleWall              => &[&[0]],
            RawTile::CastleWallCurveLeft     => &[&[0], &[]],
            RawTile::CastleWallCurveRight    => &[&[0], &[]],
            RawTile::CastleWallJunction      => &[&[0], &[], &[]],
            RawTile::CastleWallRoad          => &[&[0], &[]],
            RawTile::Cloister                => &[&[]],
            RawTile::CloisterRoad            => &[&[]],
            RawTile::Road                    => &[&[], &[]],
            RawTile::RoadCurve               => &[&[], &[]],
            RawTile::RoadJunctionLarge       => &[&[], &[], &[], &[]],
            RawTile::RoadJunctionSmall       => &[&[], &[], &[]],
            RawTile::CastleCenterEntryShield => &[&[0], &[0]],
            RawTile::CastleCenterSideShield  => &[&[0]],
            RawTile::CastleEdgeShield        => &[&[0]],
            RawTile::CastleEdgeRoadShield    => &[&[0], &[]],
            RawTile::CastleTubeShield        => &[&[0], &[0]],
        }
    }

    fn city_score(&self) -> u16 {
        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => 2,
            RawTile::CastleCenterEntry       => 1,
            RawTile::CastleCenterSide        => 1,
            RawTile::CastleEdge              => 1,
            RawTile::CastleEdgeRoad          => 1,
            RawTile::CastleSides             => 1,
            RawTile::CastleSidesEdge         => 1,
            RawTile::CastleTube              => 1,
            RawTile::CastleWall              => 1,
            RawTile::CastleWallCurveLeft     => 1,
            RawTile::CastleWallCurveRight    => 1,
            RawTile::CastleWallJunction      => 1,
            RawTile::CastleWallRoad          => 1,
            RawTile::Cloister                => 1,
            RawTile::CloisterRoad            => 1,
            RawTile::Road                    => 1,
            RawTile::RoadCurve               => 1,
            RawTile::RoadJunctionLarge       => 1,
            RawTile::RoadJunctionSmall       => 1,
            RawTile::CastleCenterEntryShield => 2,
            RawTile::CastleCenterSideShield  => 2,
            RawTile::CastleEdgeShield        => 2,
            RawTile::CastleEdgeRoadShield    => 2,
            RawTile::CastleTubeShield        => 2,
        }
    }

    fn allowed_rotations(&self) -> &'static [Rotation] {
        use Rotation::*;

        #[cfg_attr(rustfmt, rustfmt_skip)]
        match self {
            RawTile::CastleCenterShield      => &[Zero],
            RawTile::CastleCenterEntry       => &[Zero, One, Two, Three],
            RawTile::CastleCenterSide        => &[Zero, One, Two, Three],
            RawTile::CastleEdge              => &[Zero, One, Two, Three],
            RawTile::CastleEdgeRoad          => &[Zero, One, Two, Three],
            RawTile::CastleSides             => &[Zero, One],
            RawTile::CastleSidesEdge         => &[Zero, One, Two, Three],
            RawTile::CastleTube              => &[Zero, One],
            RawTile::CastleWall              => &[Zero, One, Two, Three],
            RawTile::CastleWallCurveLeft     => &[Zero, One, Two, Three],
            RawTile::CastleWallCurveRight    => &[Zero, One, Two, Three],
            RawTile::CastleWallJunction      => &[Zero, One, Two, Three],
            RawTile::CastleWallRoad          => &[Zero, One, Two, Three],
            RawTile::Cloister                => &[Zero],
            RawTile::CloisterRoad            => &[Zero, One, Two, Three],
            RawTile::Road                    => &[Zero, One],
            RawTile::RoadCurve               => &[Zero, One, Two, Three],
            RawTile::RoadJunctionLarge       => &[Zero],
            RawTile::RoadJunctionSmall       => &[Zero, One, Two, Three],
            RawTile::CastleCenterEntryShield => &[Zero, One, Two, Three],
            RawTile::CastleCenterSideShield  => &[Zero, One, Two, Three],
            RawTile::CastleEdgeShield        => &[Zero, One, Two, Three],
            RawTile::CastleEdgeRoadShield    => &[Zero, One, Two, Three],
            RawTile::CastleTubeShield        => &[Zero, One],
        }
    }

    fn has_cloister(self) -> bool {
        match self {
            RawTile::Cloister | RawTile::CloisterRoad => true,
            _ => false,
        }
    }

    fn count_without_starting_tile(self) -> usize {
        if matches!(self, RawTile::CastleWallRoad) {
            self.count() - 1
        } else {
            self.count()
        }
    }

    fn count(self) -> usize {
        match self {
            RawTile::CastleCenterShield => 1,
            RawTile::CastleCenterEntry => 1,
            RawTile::CastleCenterSide => 3,
            RawTile::CastleEdge => 3,
            RawTile::CastleEdgeRoad => 3,
            RawTile::CastleSides => 3,
            RawTile::CastleSidesEdge => 2,
            RawTile::CastleTube => 1,
            RawTile::CastleWall => 5,
            RawTile::CastleWallCurveLeft => 3,
            RawTile::CastleWallCurveRight => 3,
            RawTile::CastleWallJunction => 3,
            RawTile::CastleWallRoad => 4,
            RawTile::Cloister => 4,
            RawTile::CloisterRoad => 2,
            RawTile::Road => 8,
            RawTile::RoadCurve => 9,
            RawTile::RoadJunctionLarge => 1,
            RawTile::RoadJunctionSmall => 4,
            RawTile::CastleCenterEntryShield => 2,
            RawTile::CastleCenterSideShield => 1,
            RawTile::CastleEdgeShield => 2,
            RawTile::CastleEdgeRoadShield => 2,
            RawTile::CastleTubeShield => 2,
        }
    }
}

macro_rules! include_tile_img {
    ($name: expr) => {
        include_bytes!(concat!("../resources/tiles/", $name, ".png"))
    };
}

impl AsImg for RawTile {
    fn img_bytes(&self) -> &'static [u8] {
        match self {
            RawTile::CastleCenterShield => include_tile_img!("CastleCenterShield"),
            RawTile::CastleCenterEntry => include_tile_img!("CastleCenterEntry"),
            RawTile::CastleCenterSide => include_tile_img!("CastleCenterSide"),
            RawTile::CastleEdge => include_tile_img!("CastleEdge"),
            RawTile::CastleEdgeRoad => include_tile_img!("CastleEdgeRoad"),
            RawTile::CastleSides => include_tile_img!("CastleSides"),
            RawTile::CastleSidesEdge => include_tile_img!("CastleSidesEdge"),
            RawTile::CastleTube => include_tile_img!("CastleTube"),
            RawTile::CastleWall => include_tile_img!("CastleWall"),
            RawTile::CastleWallCurveLeft => include_tile_img!("CastleWallCurveLeft"),
            RawTile::CastleWallCurveRight => include_tile_img!("CastleWallCurveRight"),
            RawTile::CastleWallJunction => include_tile_img!("CastleWallJunction"),
            RawTile::CastleWallRoad => include_tile_img!("CastleWallRoad"),
            RawTile::Cloister => include_tile_img!("Cloister"),
            RawTile::CloisterRoad => include_tile_img!("CloisterRoad"),
            RawTile::Road => include_tile_img!("Road"),
            RawTile::RoadCurve => include_tile_img!("RoadCurve"),
            RawTile::RoadJunctionLarge => include_tile_img!("RoadJunctionLarge"),
            RawTile::RoadJunctionSmall => include_tile_img!("RoadJunctionSmall"),
            RawTile::CastleCenterEntryShield => include_tile_img!("CastleCenterEntryShield"),
            RawTile::CastleCenterSideShield => include_tile_img!("CastleCenterSideShield"),
            RawTile::CastleEdgeShield => include_tile_img!("CastleEdgeShield"),
            RawTile::CastleEdgeRoadShield => include_tile_img!("CastleEdgeRoadShield"),
            RawTile::CastleTubeShield => include_tile_img!("CastleTubeShield"),
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq)]
enum Border {
    Field,
    Road,
    City,
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq, Hash)]
enum Side {
    North = 0,
    East = 1,
    South = 2,
    West = 3,
}

impl Side {
    fn rotate_by(self, rotation: Rotation) -> Self {
        let rotated_value = ((self as u8) + (rotation as u8)) % 4;
        FromPrimitive::from_u8(rotated_value).unwrap()
    }

    fn unrotate_by(self, rotation: Rotation) -> Self {
        let rotated_value = ((self as u8 + 4) - (rotation as u8)) % 4;
        FromPrimitive::from_u8(rotated_value).unwrap()
    }

    fn idx(self) -> usize {
        self as u8 as usize
    }

    fn to_shift(self) -> (i32, i32) {
        match self {
            Side::North => (0, -1),
            Side::East => (1, 0),
            Side::South => (0, 1),
            Side::West => (-1, 0),
        }
    }

    fn opposite(self) -> Self {
        match self {
            Side::North => Side::South,
            Side::East => Side::West,
            Side::South => Side::North,
            Side::West => Side::East,
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq, Hash)]
enum Seg {
    First = 0,
    Mid = 1,
    Last = 2,
}

impl Seg {
    fn opposite(self) -> Self {
        match self {
            Seg::First => Seg::Last,
            Seg::Mid => Seg::Mid,
            Seg::Last => Seg::First,
        }
    }

    fn idx(self) -> usize {
        match self {
            Seg::First => 0,
            Seg::Mid => 1,
            Seg::Last => 2,
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq)]
enum Rotation {
    Zero = 0,
    One = 1,
    Two = 2,
    Three = 3,
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, IntoEnumIterator, PartialEq, Eq)]
pub enum Player {
    Red,
    Blue,
}

impl Player {
    fn opposite(&self) -> Self {
        match self {
            Player::Red => Player::Blue,
            Player::Blue => Player::Red,
        }
    }

    pub fn idx(&self) -> usize {
        match self {
            Player::Red => 0,
            Player::Blue => 1,
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, FromPrimitive, ToPrimitive, IntoEnumIterator, PartialEq, Eq)]
enum FeatureKind {
    Field,
    Road,
    City,
    Cloister,
}

#[derive(Debug, Clone, Copy)]
struct Meeple {
    /// Type of meeple's position, whether it's on field, road, or city
    kind: FeatureKind,
    /// Master index, which is used to index into the array of features
    index: usize,
    player: Player,
}

impl Meeple {
    fn new(kind: FeatureKind, index: usize, player: Player) -> Self {
        Self {
            kind,
            index,
            player,
        }
    }
}

impl AsImg for Meeple {
    fn img_bytes(&self) -> &'static [u8] {
        match self.player {
            Player::Red => {
                include_bytes!("../resources/misc/MeepleRed.png")
            }
            Player::Blue => {
                include_bytes!("../resources/misc/MeepleBlue.png")
            }
        }
    }
}

#[cfg(test)]
mod tile_utility_tests {
    use super::*;
    use itertools::iproduct;

    #[test]
    fn rotate_unrotate_test() {
        for (side, rotation) in iproduct!(Side::into_enum_iter(), Rotation::into_enum_iter()) {
            assert_eq!(side, side.rotate_by(rotation).unrotate_by(rotation));
        }
    }

    #[test]
    fn loc_neighbor_test() {
        for (x, y) in iproduct!(1..15, 1..15) {
            for side in Side::into_enum_iter() {
                assert!(matches!(Loc::new(x, y).neighbor_on(side), Some(_)))
            }
        }
        for x in 0..16 {
            assert!(matches!(Loc::new(x, 0).neighbor_on(Side::North), None))
        }
        for x in 0..16 {
            assert!(matches!(Loc::new(x, 15).neighbor_on(Side::South), None))
        }
        for y in 0..16 {
            assert!(matches!(Loc::new(0, y).neighbor_on(Side::West), None))
        }
        for y in 0..16 {
            assert!(matches!(Loc::new(15, y).neighbor_on(Side::East), None))
        }
    }

    #[test]
    fn side_opposite_test() {
        assert_eq!(Side::East.opposite(), Side::West);
        assert_eq!(Side::West.opposite(), Side::East);
        assert_eq!(Side::North.opposite(), Side::South);
        assert_eq!(Side::North.opposite(), Side::South);
    }

    #[test]
    fn seg_opposite_test() {
        assert_eq!(Seg::First.opposite(), Seg::Last);
        assert_eq!(Seg::Mid.opposite(), Seg::Mid);
        assert_eq!(Seg::Last.opposite(), Seg::First);
    }

    #[test]
    fn loc_surround_test() {
        let loc = Loc::new(1, 1);
        let surroundings: HashSet<Loc> = loc.surroundings_without_center().collect();
        let surroundings_t: HashSet<Loc> = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 0),
            (1, 2),
            (2, 0),
            (2, 1),
            (2, 2),
        ]
        .iter()
        .map(|&(x, y)| Loc::new(x, y))
        .collect();
        assert_eq!(surroundings, surroundings_t);
    }
}

#[cfg(test)]
mod tile_integrity_test {
    use super::*;

    #[test]
    fn rawtile_count_test() {
        let count_sum: usize = RawTile::into_enum_iter()
            .map(|raw_tile: RawTile| raw_tile.count())
            .sum();
        assert_eq!(count_sum, 72);
    }

    #[test]
    fn rawtile_borders_test() {
        const BORDER_TEST_DATA: [&'static str; 25] = [
            "----", "CCCC", "CCRC", "CCFC", "CCFF", "CCRR", "CFCF", "CFFC", "FCFC", "CFFF", "CFRR",
            "CRRF", "CRRR", "CRFR", "FFFF", "FFRF", "RFRF", "FFRR", "RRRR", "FRRR", "CCRC", "CCFC",
            "CCFF", "CCRR", "FCFC",
        ];

        for i in 1..=24 {
            let raw_tile: RawTile = FromPrimitive::from_u8(i).unwrap();
            for (border_idx, &border) in raw_tile.borders().iter().enumerate() {
                let correct_border = match BORDER_TEST_DATA[i as usize]
                    .chars()
                    .nth(border_idx)
                    .unwrap()
                {
                    'C' => Border::City,
                    'R' => Border::Road,
                    'F' => Border::Field,
                    _ => panic!("Corrupt test data"),
                };
                assert_eq!(border, correct_border);
            }
        }
    }

    #[test]
    fn tile_border_at_test() {
        // the "CFRR" tile, which is very assymetric
        let tile = Tile::new(RawTile::CastleWallCurveLeft, Rotation::One, None);

        assert_eq!(tile.border_at(Side::North), Border::Road);
        assert_eq!(tile.border_at(Side::East), Border::City);
        assert_eq!(tile.border_at(Side::South), Border::Field);
        assert_eq!(tile.border_at(Side::West), Border::Road);
    }

    #[test]
    fn tile_features_test() {
        let raw_tile = RawTile::CastleEdgeRoadShield;
        let tile = Tile::new(raw_tile, Rotation::One, None);

        println!("CITY FEATURES\n{:#?}", tile.city_features());
        println!("ROAD FEATURES\n{:#?}", tile.road_features());
        println!("FIELD FEATURES\n{:#?}", tile.field_features());

        let mut city_features = ArrayVec::<[ArrayVec<[Side; 4]>; 2]>::new();
        city_features.push([Side::East, Side::South].iter().cloned().collect());

        assert_eq!(tile.city_features(), city_features);

        let mut road_features = ArrayVec::<[ArrayVec<[Side; 2]>; 4]>::new();
        road_features.push([Side::West, Side::North].iter().cloned().collect());

        assert_eq!(tile.road_features(), road_features);

        let mut field_features = ArrayVec::<[ArrayVec<[(Side, Seg); 12]>; 4]>::new();
        field_features.push(
            [(Side::West, Seg::First), (Side::North, Seg::Last)]
                .iter()
                .cloned()
                .collect(),
        );
        field_features.push(
            [(Side::West, Seg::Last), (Side::North, Seg::First)]
                .iter()
                .cloned()
                .collect(),
        );

        assert_eq!(tile.field_features(), field_features);
    }

    #[test]
    fn state_size_test() {
        eprintln!("State size: {:?}", std::mem::size_of::<State>());
    }
}

#[allow(dead_code)]
fn game_randomplay_test() {
    let mut state = State::new();
    let mut i = 0;
    let mut draw_fail_count = 0;
    state.save_img(&format!("derived/{}.png", i)).unwrap();
    while let Some(drawn_raw_tile) = state.deck.draw() {
        i += 1;
        let legal_actions = state.get_legal_actions_from_tile(drawn_raw_tile);
        if legal_actions.len() == 0 {
            eprintln!("No legal action for this drawn tile: {:?}", drawn_raw_tile);
            state.deck.put_back(drawn_raw_tile);
            draw_fail_count += 1;
            if draw_fail_count == 10 {
                eprintln!("There's absolutely no placable tile left");
                return;
            }
            continue;
        } else {
            draw_fail_count = 0;
        }

        let chosen_action = legal_actions
            .choose_weighted(&mut thread_rng(), |action| match action.tile.meeple {
                Some(Meeple {
                    kind: FeatureKind::City,
                    ..
                }) => 5,
                Some(Meeple {
                    kind: FeatureKind::Road,
                    ..
                }) => 5,
                Some(Meeple {
                    kind: FeatureKind::Field,
                    ..
                }) => 2,
                Some(Meeple {
                    kind: FeatureKind::Cloister,
                    ..
                }) => 100,
                _ => 10,
            })
            .unwrap();
        println!("STEP {}", i);
        println!("  Chosen action: {:?}", chosen_action);
        let derived_state = state.get_state_after_action(*chosen_action);
        println!("  Scores: {:?}", derived_state.scores);
        derived_state
            .save_img(&format!("derived/{}.png", i))
            .unwrap();
        state = derived_state;
    }
}

pub mod utils {
    use super::*;
    use tensorflow as tf;

    pub fn scores_to_tensor(scores: [u16; 2], me: Player) -> tf::Tensor<f32> {
        let mut scores_vec: Vec<_> = scores.iter().cloned().map(|s| s as f32).collect();
        if me == Player::Blue {
            scores_vec.swap(0, 1);
        }
        let scores_tensor = tf::Tensor::new(&[2])
            .with_values(&scores_vec)
            .expect("Failed to construct scores tensor");
        scores_tensor
    }
}
